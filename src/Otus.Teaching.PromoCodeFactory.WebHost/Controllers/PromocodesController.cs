﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IReadWriteRepository<PromoCode> repository;
        public PromocodesController(IReadWriteRepository<PromoCode> repository)
        {
            this.repository = repository;

        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await repository.GetAllAsync();

            var model = promocodes.Select(p => new PromoCodeShortResponse()
            {
                Code = p.Code,
                ServiceInfo = p.ServiceInfo,
                BeginDate = p.BeginDate.ToShortDateString(),
                EndDate = p.EndDate.ToShortDateString(),
                PartnerName = p.PartnerName
            });

            return Ok(model);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request, [FromServices]IReadWriteRepository<Customer> customerRepository)
        {
            var customers = await customerRepository.GetAllAsync();
            var customersWithPreferences = customers.Where(c => c.CustomerPreferences.Select(s => s.Preference.Name).Contains(request.Preference));

            foreach (var customer in customersWithPreferences)
            {
                var promoCode = new PromoCode()
                {
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    EndDate = DateTime.Now.AddDays(14),
                    BeginDate = DateTime.Now,
                    CustomerId = customer.Id
                };
                await repository.AddEntity(promoCode);
            }
            return Ok();
        }
    }
}