﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IReadWriteRepository<Customer> _customerRepository;
        public CustomersController(IReadWriteRepository<Customer> repository)
        {
            this._customerRepository = repository;
        }
        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var result = customers
                        .Select(c => new CustomerShortResponse()
                        {
                            FirstName = c.FirstName,
                            LastName = c.LastName,
                            Email = c.Email,
                            Id = c.Id,
                            Preferences = c.CustomerPreferences.Select(
                                        b =>
                                            new PreferenceResponse()
                                            {
                                                Id = b.PreferenceId,
                                                Name = b.Preference.Name
                                            })
                        });
            return Ok(result);
        }
        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            return Ok(new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            });
        }
        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };
            await _customerRepository.AddEntity(customer);
            return Ok();
        }
        /// <summary>
        /// Обновить данные клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <param name="preferenceRepository"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request,
                [FromServices] IRepository<Preference> preferenceRepository)
        {
            var allPreferences = await preferenceRepository.GetAllAsync();
            var preferences = allPreferences.Where(p => request.PreferenceIds.Contains(p.Id));
            var customerPreferences = new List<CustomerPreference>();
            foreach (var item in preferences)
            {
                customerPreferences.Add(new CustomerPreference() { CustomerId = id, PreferenceId = item.Id });
            }
            var customer = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = customerPreferences
            };
            await _customerRepository.UpdateEntity(id, customer);

            return Ok();
        }
        /// <summary>
        /// Удалить данные клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customerRepository.RemoveEntity(id);
            return Ok();
        }
    }
}