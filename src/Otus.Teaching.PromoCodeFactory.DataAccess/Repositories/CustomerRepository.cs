using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository: IReadWriteRepository<Customer>
    {
        private readonly PromoDbContext dbContext;
        public CustomerRepository(PromoDbContext dbContext)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<Customer> AddEntity(Customer entity)
        {
            await dbContext.Customers.AddAsync(entity);
            await dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await dbContext.Customers
                    .Include(p => p.CustomerPreferences)
                    .ThenInclude(p=>p.Preference)
                    .ToListAsync();
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            return await dbContext.Customers
                    .Include(p => p.CustomerPreferences)
                    .ThenInclude(p => p.Preference)
                    .FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task RemoveEntity(Guid id)
        {
            var customer = await dbContext.Customers.FirstOrDefaultAsync(c => c.Id == id);
            if(customer == null) throw new ArgumentException("There is no customer with specified id");

            dbContext.Customers.Remove(customer);

            await dbContext.SaveChangesAsync();

        }

        public async Task UpdateEntity(Guid id, Customer entity)
        {
            var dbEntity = await dbContext.Customers.Include(s=>s.CustomerPreferences).FirstOrDefaultAsync(p => p.Id == id);
            if(dbEntity == null) throw new ArgumentException("There is no customer with specified id");

            dbEntity.FirstName = entity.FirstName;
            dbEntity.LastName = entity.LastName;
            dbEntity.Email = entity.Email;
            dbEntity.CustomerPreferences = new List<CustomerPreference>();
           
            foreach (var item in entity.CustomerPreferences)
            {
                dbEntity.CustomerPreferences.Add(item);
            }
            await dbContext.SaveChangesAsync();
        }
    }
}