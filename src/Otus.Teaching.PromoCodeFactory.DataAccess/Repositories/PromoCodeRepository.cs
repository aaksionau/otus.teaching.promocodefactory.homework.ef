using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromoCodeRepository : IReadWriteRepository<PromoCode>
    {
        private readonly PromoDbContext dbContext;
        public PromoCodeRepository(PromoDbContext dbContext)
        {
            this.dbContext = dbContext;

        }
        public async Task<PromoCode> AddEntity(PromoCode entity)
        {
            await dbContext.Promocodes.AddAsync(entity);
            await dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<PromoCode>> GetAllAsync()
        {
            return await dbContext.Promocodes.ToListAsync();
        }

        public Task<PromoCode> GetByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task RemoveEntity(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateEntity(Guid id, PromoCode entity)
        {
            throw new NotImplementedException();
        }
    }
}