using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class PromoDbContext : DbContext
    {
        public PromoDbContext(DbContextOptions<PromoDbContext> options):base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Employee
            modelBuilder.Entity<Employee>().HasKey(e => e.Id);
            modelBuilder.Entity<Employee>().Property(e => e.FirstName).IsRequired().HasMaxLength(125);
            modelBuilder.Entity<Employee>().Property(e => e.LastName).IsRequired().HasMaxLength(125);
            modelBuilder.Entity<Employee>().Property(e => e.Email).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Employee>().HasOne(e => e.Role).WithMany(r => r.Employees);

            //Role
            modelBuilder.Entity<Role>().HasKey(r => r.Id);
            modelBuilder.Entity<Role>().Property(r => r.Name).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Role>().Property(r => r.Description).HasMaxLength(1024);


            //Customer
            modelBuilder.Entity<Customer>().HasKey(r => r.Id);
            modelBuilder.Entity<Customer>().Property(r => r.FirstName).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Customer>().Property(r => r.LastName).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Customer>().Property(r => r.Email).IsRequired().HasMaxLength(64);

            //Preference
            modelBuilder.Entity<Preference>().HasKey(p => p.Id);
            modelBuilder.Entity<Preference>().Property(p => p.Name).IsRequired().HasMaxLength(64);

            //Promocode
            modelBuilder.Entity<PromoCode>().HasKey(p => p.Id);
            modelBuilder.Entity<PromoCode>().Property(p => p.Code).IsRequired().HasMaxLength(32);
            modelBuilder.Entity<PromoCode>().Property(p => p.ServiceInfo).HasMaxLength(256);
            modelBuilder.Entity<PromoCode>().Property(p => p.PartnerName).IsRequired().HasMaxLength(64);

            //Relationship between Employee and Role
            modelBuilder.Entity<Employee>()
                        .HasOne(p => p.Role)
                        .WithMany(p => p.Employees)
                        .HasForeignKey(p => p.RoleId);

            //Relationships between Customer and Promocode (one-to-many)
            modelBuilder.Entity<Customer>()
                            .HasMany(r => r.PromoCodes)
                            .WithOne(e => e.Customer)
                            .HasForeignKey(e => e.CustomerId)
                            .OnDelete(DeleteBehavior.Cascade);

            //Relationships between customer and preferences (many-to-many)
            modelBuilder.Entity<CustomerPreference>().HasKey(p => new { p.PreferenceId, p.CustomerId });
            modelBuilder.Entity<CustomerPreference>()
                                .HasOne(p => p.Customer)
                                .WithMany(p => p.CustomerPreferences)
                                .HasForeignKey(p => p.CustomerId)
                                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CustomerPreference>()
                                .HasOne(p => p.Preference)
                                .WithMany(p => p.CustomerPreferences)
                                .HasForeignKey(p => p.PreferenceId);

            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<PromoCode> Promocodes { get; set; }

        public DbSet<Preference> Preference { get; set; }
        
        
    }
}